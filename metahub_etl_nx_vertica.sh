#!/bin/bash
# File: metahub_etl_nx_vertica.sh
# Author: John B Damask
# Created: July 15, 2016
# Purpose: Copy data from Metahub (Oracle) to Metahub (Vertica)
# Notes: Requires both vsql and sqlplus clients with defined aliases, batchQueryOracleThreads.py (which 
#        depends on Python 2.x w/ cx_Oracle and ~/params.py file), and a TABLES file that 
#        contains metahub table names...whew! 
#        Also, this needs to be run by a user with the Vertica role, DMPDB_ADMIN
# ToDo:  Dependency on sqlplus is silly. Change to python cx_Oracle at some point in the future
# Arguments: None
# Synopsis: ./metahub_etl_nx_vertica.sh

#---- Configure aliases for vsql and sqlplus
shopt -s expand_aliases
source ~/.bash_aliases

#---- Explicit directory needed to run on crontab
cd /db/dmp/dice-metahub-etl

#---- Clean up
rm *.log *.csv *.sql *.txt
vnx -c 'set role dmpdb_admin; drop table dmp_scratch.metahub_rejected'

#---- Create SELECT statements
cat TABLES | while read x; do echo "select * from metahub."$x >> dump_oracle_metahub.sql; done

#---- Save record counts from Oracle
cat TABLES | while read x; do metahub_o <<< "select 'metahub."$x"' as table_name, count(*) from metahub."$x";" >> oracle_counts.txt; done
grep 'metahub' oracle_counts.txt | sed -r 's/\s+/|/g' | sort > oracle_metahub_counts.txt

#---- Dump Metahub records to disk
python batchQueryOracleThreads.py metahub dump_oracle_metahub.sql

#---- Truncate Vertica scratch tables
cat TABLES |  while read x ; do sql=`echo "set role dmpdb_admin; truncate table dmp_scratch."$x";"`; vnx -Atc "${sql}"; done

#---- Insert records into Vertica scratch tables
cat TABLES |  while read x ; do cmd=`echo "set role dmpdb_admin; copy dmp_scratch."$x" from STDIN TRAILING NULLCOLS NO ESCAPE SKIP 1 REJECTED DATA AS TABLE dmp_scratch.METAHUB_REJECTED"`; cat "$x".csv | vnx -c "${cmd}"; done

#---- Count loaded records
cat TABLES |  while read x ; do sql=`echo "set role dmpdb_admin; select '''dmp_scratch."$x"''' as tbl, count(*) from dmp_scratch."$x";"`; vnx -Atc "${sql}" >> vertica_metahub_staging_counts.txt; done

#---- Validate that records loaded match expected
ch1=$(diff <(cut -d'.' -f2 oracle_metahub_counts.txt | sort) <(cut -d'.' -f2 vertica_metahub_staging_counts.txt | tr -d "'" | sort))
if [ "${ch1}" != "" ];then
    echo "Staging check failed. ETL aborted"
    mail -s "metahub_etl_nx_vertica ERROR - Staging check failed. ETL aborted" john.damask@novartis.com < /dev/null
    exit 1
fi

#---- Back up production data and truncate tables
cat TABLES | while read x; do sql=$(echo "set role dmpdb_admin; truncate table dmp_scratch."$x"_bk;"); vnx -Atc "$sql"; done
cat TABLES | while read x; do sql=$(echo "set role dmpdb_admin; set role metahub_owner; insert into dmp_scratch."$x"_bk select * from metahub."$x";commit;"); vnx -Atc "$sql"; done
cat TABLES | while read x; do sql=$(echo "set role metahub_owner; set role dmpdb_admin; truncate table metahub."$x";"); vnx -Atc "$sql"; done

#---- Copy data from scratch to production
cat TABLES | while read x; do sql=$(echo "set role metahub_owner;set role dmpdb_admin; insert /*+ direct */ into metahub."$x" select * from dmp_scratch."$x";commit;"); vnx -Atc "$sql"; done

#---- Check record counts
cat TABLES | while read x; do sql=$(echo "set role metahub_owner; set role dmpdb_admin; select '''metahub."$x"''' as tbl, count(*) from metahub."$x";"); vnx -Atc "$sql" >> prod_counts.txt; done
ch2=$(diff <(cut -d'.' -f2 prod_counts.txt) <(cut -d'.' -f2 vertica_metahub_staging_counts.txt))
if [ "${ch2}" != "" ];then
    echo "Production load ERROR!. Inconsistent data between staging and production area"
    echo "There may be dirty data in Metahub. Run this command to see issue:"
    echo "diff <(vnx -Atf prod_counts.txt | cut -d'.' -f2) <(cat vertica_metahub_staging_counts.txt | cut -d'.' -f2 |tr -d \"'\" | sort)"
    cat TABLES | while read x; do sql=$(echo "set role metahub_owner; set role dmpdb_admin; truncate table metahub."$x";"); vnx -Atc "$sql"; done
    cat TABLES | while read x; do sql=$(echo "set role metahub_owner;set role dmpdb_admin; insert /*+ direct */ into metahub."$x" select * from dmp_scratch."$x"_bk;commit;"); vnx -Atc "$sql"; done
    mail -s "metahub_etl_nx_vertica ERROR - Inconsistent data between staging and production area. Rolling back" john.damask@novartis.com < /dev/null
    exit 1
fi

mail -s "metahub_etl_nx_vertica has completed" john.damask@novartis.com < /dev/null
