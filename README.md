# README #

Code to copy Metahub data from it's source Oracle database to Vertica. For full information see [Confluence](http://web.global.nibr.novartis.net/apps/confluence/display/QB/ETL+-+Oracle+to+Vertica)

### Why do we need this? ###

* DRX saves data to Oracle whereas analysts typically use Vertica to access integrated DMP data. 
* 2.0 (The first version used SQOOP and was retired in July 2016. Nothing was reused so don't bother looking for the code)

### How does it work? ###

* From Unix /db/dmp, git clone https://bitbucket.org/damasjo1/dice-metahub-etl
* Set permissions, configure aliases and dependencies per source code instructions
* Needs Bash + Python (<3 due to cx_Oracle dependency) + vsql + sqlplus
* Requires Bash aliases, "vnx" and "metahub_o" for Vertica and Oracle connections
* Run ./metahub_etl_nx_vertica.sh 

### Who do I talk to? ###

* John Damask